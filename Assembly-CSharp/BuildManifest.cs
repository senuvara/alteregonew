using System;
using UnityEngine;

[Serializable]
public class BuildManifest
{
	[SerializeField]
	private string scmCommitId;

	[SerializeField]
	private string scmBranch;

	[SerializeField]
	private string buildNumber;

	[SerializeField]
	private string buildStartTime = DateTime.Now.ToUniversalTime().ToString();

	[SerializeField]
	private string projectId;

	[SerializeField]
	private string bundleId;

	[SerializeField]
	private string unityVersion;

	[SerializeField]
	private string xcodeVersion;

	[SerializeField]
	private string cloudBuildTargetName;

	public string ScmCommitId => scmCommitId;

	public string ScmBranch => scmBranch;

	public string BuildNumber => buildNumber;

	public string BuildStartTime => buildStartTime;

	public string ProjectId => projectId;

	public string BundleId => bundleId;

	public string UnityVersion => unityVersion;

	public string XCodeVersion => xcodeVersion;

	public string CloudBuildTargetName => cloudBuildTargetName;

	public static BuildManifest Load()
	{
		TextAsset textAsset = Resources.Load<TextAsset>("UnityCloudBuildManifest.json");
		if (textAsset == null)
		{
			textAsset = Resources.Load<TextAsset>("UnityCloudBuildManifestLocal.json");
			if (textAsset == null)
			{
				return new BuildManifest();
			}
			return JsonUtility.FromJson<BuildManifest>(textAsset.text);
		}
		return JsonUtility.FromJson<BuildManifest>(textAsset.text);
	}
}
