using App;
using System;
using System.Collections.Generic;
using UnityEngine;

public class AnalyticsManager
{
	private static readonly string DeviceInfoKey = "DeviceInfo";

	private static string GetDeviceInfo()
	{
		if (!PlayerPrefs.HasKey(DeviceInfoKey))
		{
			List<string> list = new List<string>();
			string item = Guid.NewGuid().ToString().ToUpper();
			list.Add(item);
			list.Add(SystemInfo.operatingSystem);
			list.Add(SystemInfo.deviceModel);
			list.Add(Screen.height + "x" + Screen.width);
			list.Add(BuildInfo.APP_VERSION);
			PlayerPrefs.SetString(DeviceInfoKey, string.Join("\t", list.ToArray()));
			PlayerPrefs.Save();
		}
		return PlayerPrefs.GetString(DeviceInfoKey);
	}

	public static string GetUserID()
	{
		return GetDeviceInfo().Split('\t')[0].Replace("/editor", "").Replace("/debug", "");
	}

	public static void SendEvent(string[] eventDataList, string ssid = "1DLWkIkaBFBnrV9VoNi09qwvOhocUADC8BWVHfNzyjfA")
	{
		if (!PlayerPrefs.HasKey(DeviceInfoKey))
		{
			GetDeviceInfo();
		}
		string str = "\t";
		str = str + DateTime.Now.ToString() + "\t";
		str = str + GetDeviceInfo() + "\t";
		str = str + BuildInfo.APP_VERSION + "\t";
		str += string.Join("\t", eventDataList);
		str += "\t";
		string[] statusStringArray = StringStatusConverter.GetStatusStringArray(null, showHideItem: false);
		str += statusStringArray[1];
		str = str + SafeAreaAdjuster.GetScreenSize().ToString() + "\t";
		str = str + Screen.safeArea.ToString() + "\t";
		str += SafeAreaAdjuster.SafeAreaHeight;
		GssDataHelper.PostData(ssid, "EGO_005", str);
	}
}
