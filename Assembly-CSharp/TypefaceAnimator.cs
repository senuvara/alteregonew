using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
[AddComponentMenu("UI/Effects/TypefaceAnimator")]
public class TypefaceAnimator : BaseMeshEffect
{
	public enum TimeMode
	{
		Time,
		Speed
	}

	public enum Style
	{
		Once,
		Loop,
		PingPong
	}

	public TimeMode timeMode;

	public float duration = 1f;

	public float speed = 5f;

	public float delay;

	public Style style;

	public bool playOnAwake = true;

	[SerializeField]
	private float m_progress = 1f;

	public bool usePosition;

	public bool useRotation;

	public bool useScale;

	public bool useAlpha;

	public bool useColor;

	public UnityEvent onStart;

	public UnityEvent onComplete;

	[SerializeField]
	private int characterNumber;

	private float animationTime;

	private Coroutine playCoroutine;

	private bool m_isPlaying;

	public Vector3 positionFrom = Vector3.zero;

	public Vector3 positionTo = Vector3.zero;

	public AnimationCurve positionAnimationCurve = AnimationCurve.Linear(0f, 0f, 1f, 1f);

	public float positionSeparation = 0.5f;

	public float rotationFrom;

	public float rotationTo;

	public Vector2 rotationPivot = new Vector2(0.5f, 0.5f);

	public AnimationCurve rotationAnimationCurve = AnimationCurve.Linear(0f, 0f, 1f, 1f);

	public float rotationSeparation = 0.5f;

	public bool scaleSyncXY = true;

	public float scaleFrom;

	public float scaleTo = 1f;

	public Vector2 scalePivot = new Vector2(0.5f, 0.5f);

	public AnimationCurve scaleAnimationCurve = AnimationCurve.Linear(0f, 0f, 1f, 1f);

	public float scaleFromY;

	public float scaleToY = 1f;

	public Vector2 scalePivotY = new Vector2(0.5f, 0.5f);

	public AnimationCurve scaleAnimationCurveY = AnimationCurve.Linear(0f, 0f, 1f, 1f);

	public float scaleSeparation = 0.5f;

	public float alphaFrom;

	public float alphaTo = 1f;

	public AnimationCurve alphaAnimationCurve = AnimationCurve.Linear(0f, 0f, 1f, 1f);

	public float alphaSeparation = 0.5f;

	public Color colorFrom = Color.white;

	public Color colorTo = Color.white;

	public AnimationCurve colorAnimationCurve = AnimationCurve.Linear(0f, 0f, 1f, 1f);

	public float colorSeparation = 0.5f;

	public float progress
	{
		get
		{
			return m_progress;
		}
		set
		{
			m_progress = value;
			if (base.graphic != null)
			{
				base.graphic.SetVerticesDirty();
			}
		}
	}

	public bool isPlaying => m_isPlaying;

	protected override void OnEnable()
	{
		if (playOnAwake)
		{
			Play();
		}
		base.OnEnable();
	}

	protected override void OnDisable()
	{
		Stop();
		base.OnDisable();
	}

	public void Play()
	{
		progress = 0f;
		switch (timeMode)
		{
		case TimeMode.Time:
			animationTime = duration;
			break;
		case TimeMode.Speed:
			animationTime = (float)characterNumber / 10f / speed;
			break;
		}
		switch (style)
		{
		case Style.Once:
			playCoroutine = StartCoroutine(PlayOnceCoroutine());
			break;
		case Style.Loop:
			playCoroutine = StartCoroutine(PlayLoopCoroutine());
			break;
		case Style.PingPong:
			playCoroutine = StartCoroutine(PlayPingPongCoroutine());
			break;
		}
	}

	public void Stop()
	{
		if (playCoroutine != null)
		{
			StopCoroutine(playCoroutine);
		}
		m_isPlaying = false;
		playCoroutine = null;
	}

	private IEnumerator PlayOnceCoroutine()
	{
		if (delay > 0f)
		{
			yield return new WaitForSeconds(delay);
		}
		if (!m_isPlaying)
		{
			m_isPlaying = true;
			if (onStart != null)
			{
				onStart.Invoke();
			}
			while (progress < 1f)
			{
				progress += Time.deltaTime / animationTime;
				yield return null;
			}
			m_isPlaying = false;
			progress = 1f;
			if (onComplete != null)
			{
				onComplete.Invoke();
			}
		}
	}

	private IEnumerator PlayLoopCoroutine()
	{
		if (delay > 0f)
		{
			yield return new WaitForSeconds(delay);
		}
		if (m_isPlaying)
		{
			yield break;
		}
		m_isPlaying = true;
		if (onStart != null)
		{
			onStart.Invoke();
		}
		while (true)
		{
			progress += Time.deltaTime / animationTime;
			if (progress > 1f)
			{
				progress -= 1f;
			}
			yield return null;
		}
	}

	private IEnumerator PlayPingPongCoroutine()
	{
		if (delay > 0f)
		{
			yield return new WaitForSeconds(delay);
		}
		if (m_isPlaying)
		{
			yield break;
		}
		m_isPlaying = true;
		if (onStart != null)
		{
			onStart.Invoke();
		}
		bool isPositive = true;
		while (true)
		{
			float num = Time.deltaTime / animationTime;
			if (isPositive)
			{
				progress += num;
				if (progress > 1f)
				{
					isPositive = false;
					progress -= num;
				}
			}
			else
			{
				progress -= num;
				if (progress < 0f)
				{
					isPositive = true;
					progress += num;
				}
			}
			yield return null;
		}
	}

	public override void ModifyMesh(VertexHelper vertexHelper)
	{
		if (!IsActive() || vertexHelper.currentVertCount == 0)
		{
			return;
		}
		List<UIVertex> list = new List<UIVertex>();
		vertexHelper.GetUIVertexStream(list);
		List<UIVertex> list2 = new List<UIVertex>();
		for (int i = 0; i < list.Count; i++)
		{
			int num = i % 6;
			if (num == 0 || num == 1 || num == 2 || num == 4)
			{
				list2.Add(list[i]);
			}
		}
		ModifyVertices(list2);
		List<UIVertex> list3 = new List<UIVertex>(list.Count);
		for (int j = 0; j < list.Count / 6; j++)
		{
			int num2 = j * 4;
			list3.Add(list2[num2]);
			list3.Add(list2[num2 + 1]);
			list3.Add(list2[num2 + 2]);
			list3.Add(list2[num2 + 2]);
			list3.Add(list2[num2 + 3]);
			list3.Add(list2[num2]);
		}
		vertexHelper.Clear();
		vertexHelper.AddUIVertexTriangleStream(list3);
	}

	public void ModifyVertices(List<UIVertex> verts)
	{
		if (IsActive())
		{
			Modify(verts);
		}
	}

	private void Modify(List<UIVertex> verts)
	{
		characterNumber = verts.Count / 4;
		int num = 0;
		for (int i = 0; i < verts.Count; i++)
		{
			if (i % 4 != 0)
			{
				continue;
			}
			num = i / 4;
			UIVertex value = verts[i];
			UIVertex value2 = verts[i + 1];
			UIVertex value3 = verts[i + 2];
			UIVertex value4 = verts[i + 3];
			if (usePosition)
			{
				float d = positionAnimationCurve.Evaluate(SeparationRate(progress, num, characterNumber, positionSeparation));
				Vector3 vector = (positionTo - positionFrom) * d + positionFrom;
				value.position += vector;
				value2.position += vector;
				value3.position += vector;
				value4.position += vector;
			}
			if (useScale)
			{
				if (scaleSyncXY)
				{
					float num2 = scaleAnimationCurve.Evaluate(SeparationRate(progress, num, characterNumber, scaleSeparation));
					float d2 = (scaleTo - scaleFrom) * num2 + scaleFrom;
					float x = (value2.position.x - value4.position.x) * scalePivot.x + value4.position.x;
					float y = (value2.position.y - value4.position.y) * scalePivot.y + value4.position.y;
					Vector3 b = new Vector3(x, y, 0f);
					value.position = (value.position - b) * d2 + b;
					value2.position = (value2.position - b) * d2 + b;
					value3.position = (value3.position - b) * d2 + b;
					value4.position = (value4.position - b) * d2 + b;
				}
				else
				{
					float num3 = scaleAnimationCurve.Evaluate(SeparationRate(progress, num, characterNumber, scaleSeparation));
					float d3 = (scaleTo - scaleFrom) * num3 + scaleFrom;
					float x2 = (value2.position.x - value4.position.x) * scalePivot.x + value4.position.x;
					float y2 = (value2.position.y - value4.position.y) * scalePivot.y + value4.position.y;
					Vector3 b2 = new Vector3(x2, y2, 0f);
					value.position = new Vector3(((value.position - b2) * d3 + b2).x, value.position.y, value.position.z);
					value2.position = new Vector3(((value2.position - b2) * d3 + b2).x, value2.position.y, value2.position.z);
					value3.position = new Vector3(((value3.position - b2) * d3 + b2).x, value3.position.y, value3.position.z);
					value4.position = new Vector3(((value4.position - b2) * d3 + b2).x, value4.position.y, value4.position.z);
					num3 = scaleAnimationCurveY.Evaluate(SeparationRate(progress, num, characterNumber, scaleSeparation));
					d3 = (scaleToY - scaleFromY) * num3 + scaleFromY;
					x2 = (value2.position.x - value4.position.x) * scalePivotY.x + value4.position.x;
					y2 = (value2.position.y - value4.position.y) * scalePivotY.y + value4.position.y;
					b2 = new Vector3(x2, y2, 0f);
					value.position = new Vector3(value.position.x, ((value.position - b2) * d3 + b2).y, value.position.z);
					value2.position = new Vector3(value2.position.x, ((value2.position - b2) * d3 + b2).y, value2.position.z);
					value3.position = new Vector3(value3.position.x, ((value3.position - b2) * d3 + b2).y, value3.position.z);
					value4.position = new Vector3(value4.position.x, ((value4.position - b2) * d3 + b2).y, value4.position.z);
				}
			}
			if (useRotation)
			{
				float num4 = rotationAnimationCurve.Evaluate(SeparationRate(progress, num, characterNumber, rotationSeparation));
				float angle = (rotationTo - rotationFrom) * num4 + rotationFrom;
				float x3 = (value2.position.x - value4.position.x) * rotationPivot.x + value4.position.x;
				float y3 = (value2.position.y - value4.position.y) * rotationPivot.y + value4.position.y;
				Vector3 b3 = new Vector3(x3, y3, 0f);
				value.position = Quaternion.AngleAxis(angle, Vector3.forward) * (value.position - b3) + b3;
				value2.position = Quaternion.AngleAxis(angle, Vector3.forward) * (value2.position - b3) + b3;
				value3.position = Quaternion.AngleAxis(angle, Vector3.forward) * (value3.position - b3) + b3;
				value4.position = Quaternion.AngleAxis(angle, Vector3.forward) * (value4.position - b3) + b3;
			}
			Color c = value.color;
			if (useColor)
			{
				float b4 = colorAnimationCurve.Evaluate(SeparationRate(progress, num, characterNumber, colorSeparation));
				c = (colorTo - colorFrom) * b4 + colorFrom;
				value.color = (value2.color = (value3.color = (value4.color = c)));
			}
			if (useAlpha)
			{
				float num5 = alphaAnimationCurve.Evaluate(SeparationRate(progress, num, characterNumber, alphaSeparation));
				float num6 = (alphaTo - alphaFrom) * num5 + alphaFrom;
				c = new Color(c.r, c.g, c.b, c.a * num6);
				value.color = (value2.color = (value3.color = (value4.color = c)));
			}
			verts[i] = value;
			verts[i + 1] = value2;
			verts[i + 2] = value3;
			verts[i + 3] = value4;
		}
	}

	private static float SeparationRate(float progress, int currentCharacterNumber, int characterNumber, float separation)
	{
		return Mathf.Clamp01((progress - (float)currentCharacterNumber * separation / (float)characterNumber) / (separation / (float)characterNumber + 1f - separation));
	}
}
