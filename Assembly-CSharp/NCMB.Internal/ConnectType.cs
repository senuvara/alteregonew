namespace NCMB.Internal
{
	internal enum ConnectType
	{
		GET,
		POST,
		PUT,
		DELETE
	}
}
