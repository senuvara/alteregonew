using App;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AdManager : MonoBehaviour
{
	private static AdManager Instance;

	private bool IsActive;

	private GameObject BannerBG;

	private AndroidJavaObject AndroidUtil;

	private List<AdModule.AdEventArgs> SuspendEvent = new List<AdModule.AdEventArgs>();

	private IDictionary<string, AdModule> AdList = new Dictionary<string, AdModule>();

	private Action<bool> AppCallback;

	private void Awake()
	{
		Instance = this;
		IsActive = true;
		SetActive(AdInfo.ENABLE);
		AndroidUtil = new AndroidJavaObject("com.caracolu.appcommon.Util");
		AndroidUtil.Call("SetUIChangeListener");
	}

	public static void SetActive(bool active)
	{
		Debug.Log("AdManager#SetActive " + active);
		foreach (AdModule value in Instance.AdList.Values)
		{
			value.Destroy();
		}
		Instance.AdList.Clear();
		UnityEngine.Object.Destroy(Instance.BannerBG);
		Instance.BannerBG = null;
		if (active)
		{
			Instance.RestoreModule();
		}
		else
		{
			Instance.AddModule("SkipReward", null);
		}
		SafeAreaAdjuster.UpdateLayout();
	}

	private void RestoreModule()
	{
		foreach (string key in AdInfo.AD_ID.Keys)
		{
			AddModule(key, AdInfo.AD_ID[key]);
		}
		GameObject gameObject = Resources.Load<GameObject>("AdCanvas");
		if (gameObject != null)
		{
			BannerBG = UnityEngine.Object.Instantiate(gameObject, base.transform);
			BannerBG.SetActive(value: false);
		}
	}

	private void AddModule(string key, string[] values)
	{
		if (AdList.ContainsKey(key))
		{
			AdList[key].Destroy();
			AdList[key].OnAdEvent -= OnAdEvent;
			AdList.Remove(key);
		}
		AdModule adModule = (!key.Contains("UnityAds")) ? new AdModule(this, key, values) : new AdModuleUnityAds();
		AdList.Add(key, adModule);
		adModule.OnAdEvent += OnAdEvent;
	}

	private void OnAdEvent(object sender, AdModule.AdEventArgs args)
	{
		Debug.Log("AdManager#OnEvent(" + args.Type + " " + args.Message + ")");
		if (!IsActive)
		{
			SuspendEvent.Add(args);
			return;
		}
		string message = args.Message;
		switch (_003CPrivateImplementationDetails_003E.ComputeStringHash(message))
		{
		case 1300762695u:
			if (message == "OnReward")
			{
				AppCallback(obj: true);
			}
			break;
		case 4059217573u:
			if (message == "NoReward")
			{
				AppCallback(obj: false);
			}
			break;
		case 1299922320u:
			_ = (message == "OnClosed");
			break;
		case 489681732u:
			_ = (message == "OnFailed(Play)");
			break;
		case 1600958328u:
			if (!(message == "NavigationBar:IsVisible"))
			{
				break;
			}
			goto IL_0122;
		case 287701475u:
			if (!(message == "NavigationBar:NotVisible"))
			{
				break;
			}
			goto IL_0122;
		case 1215464327u:
			{
				if (!(message == "OnChangedSafeInsetArea"))
				{
					break;
				}
				SafeAreaAdjuster.SafeAreaHeight = AndroidUtil.GetStatic<int>("sSafeInsetHeight");
				string[] array = AdList.Keys.ToArray();
				foreach (string text in array)
				{
					if (text.Contains("Rectangle"))
					{
						AddModule(text, AdInfo.AD_ID[text]);
					}
				}
				break;
			}
			IL_0122:
			if (AdList.ContainsKey("AdMobBanner") && AdList["AdMobBanner"].IsShowing)
			{
				AdList["AdMobBanner"].Hide();
				AdList["AdMobBanner"].Show();
				SafeAreaAdjuster.UpdateLayout();
			}
			break;
		}
	}

	private void OnApplicationPause(bool pause)
	{
		Debug.Log("AdManager#OnApplicationPause " + pause);
		IsActive = !pause;
		AndroidUtil.SetStatic("sIsActive", IsActive);
		if (IsActive)
		{
			foreach (AdModule.AdEventArgs item in SuspendEvent)
			{
				OnAdEvent(null, item);
			}
			SuspendEvent.Clear();
			SafeAreaAdjuster.UpdateLayout();
		}
	}

	private void OnDestroy()
	{
		foreach (AdModule value in AdList.Values)
		{
			value.OnAdEvent -= OnAdEvent;
			value.Destroy();
		}
		Instance = null;
		AndroidUtil.Dispose();
	}

	public static bool IsReady(string type)
	{
		if (Instance == null)
		{
			return false;
		}
		foreach (string key in Instance.AdList.Keys)
		{
			if (key.Contains(type) && Instance.AdList[key].IsReady())
			{
				return true;
			}
		}
		return false;
	}

	public static void Show(string type, Action<bool> callback = null)
	{
		if (!(Instance == null))
		{
			Instance.AppCallback = callback;
			foreach (string key in Instance.AdList.Keys)
			{
				if (key.Contains(type))
				{
					AdModule adModule = Instance.AdList[key];
					if (!type.Contains("Reward") || adModule.IsReady())
					{
						adModule.Show();
						break;
					}
				}
			}
			if (!(Instance.BannerBG == null) && type.Contains("Banner"))
			{
				Instance.BannerBG.SetActive(value: true);
			}
		}
	}

	public static void Hide(string type)
	{
		if (!(Instance == null))
		{
			foreach (string key in Instance.AdList.Keys)
			{
				if (key.Contains(type))
				{
					Instance.AdList[key].Hide();
					break;
				}
			}
			if (!(Instance.BannerBG == null) && type.Contains("Banner"))
			{
				Instance.BannerBG.SetActive(value: false);
			}
		}
	}

	public void OnEventFromAndroid(string message)
	{
		AdModule.AdEventArgs adEventArgs = new AdModule.AdEventArgs();
		adEventArgs.Type = "AndroidEvent";
		adEventArgs.Message = message;
		OnAdEvent(null, adEventArgs);
	}
}
