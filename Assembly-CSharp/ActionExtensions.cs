using System;

public static class ActionExtensions
{
	public static void NullSafe<T>(this Action<T> action, T arg)
	{
		action?.Invoke(arg);
	}

	public static void NullSafe<T1, T2>(this Action<T1, T2> action, T1 arg1, T2 arg2)
	{
		action?.Invoke(arg1, arg2);
	}
}
