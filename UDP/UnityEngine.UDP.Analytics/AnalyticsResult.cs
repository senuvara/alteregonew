namespace UnityEngine.UDP.Analytics
{
	public enum AnalyticsResult
	{
		kOk,
		kError,
		kNotInitialized,
		kInvalidData
	}
}
