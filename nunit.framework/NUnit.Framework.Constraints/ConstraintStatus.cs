namespace NUnit.Framework.Constraints
{
	public enum ConstraintStatus
	{
		Unknown,
		Success,
		Failure,
		Error
	}
}
