namespace NUnit.Framework.Constraints
{
	public enum ToleranceMode
	{
		Unset,
		Linear,
		Percent,
		Ulps
	}
}
