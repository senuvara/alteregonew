using NUnit.Framework.Interfaces;
using System;

namespace NUnit.Framework.Internal.Filters
{
	[Serializable]
	public class MethodNameFilter : ValueMatchFilter
	{
		protected override string ElementName => "method";

		public MethodNameFilter(string expectedValue)
			: base(expectedValue)
		{
		}

		public override bool Match(ITest test)
		{
			return Match(test.MethodName);
		}
	}
}
