using NUnit.Framework.Interfaces;
using System;

namespace NUnit.Framework.Internal.Filters
{
	[Serializable]
	public class FullNameFilter : ValueMatchFilter
	{
		protected override string ElementName => "test";

		public FullNameFilter(string expectedValue)
			: base(expectedValue)
		{
		}

		public override bool Match(ITest test)
		{
			return Match(test.FullName);
		}
	}
}
