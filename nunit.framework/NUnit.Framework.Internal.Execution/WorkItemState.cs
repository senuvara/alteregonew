namespace NUnit.Framework.Internal.Execution
{
	public enum WorkItemState
	{
		Ready,
		Running,
		Complete
	}
}
