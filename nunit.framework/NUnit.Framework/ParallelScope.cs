using System;

namespace NUnit.Framework
{
	[Flags]
	public enum ParallelScope
	{
		None = 0x0,
		Self = 0x1,
		Children = 0x2,
		Fixtures = 0x4
	}
}
