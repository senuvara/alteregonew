using System;

namespace NUnit.Framework
{
	[Flags]
	public enum ActionTargets
	{
		Default = 0x0,
		Test = 0x1,
		Suite = 0x2
	}
}
