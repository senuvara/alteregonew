using NUnit.Framework.Interfaces;
using System.Diagnostics;

namespace NUnit.Framework.Internal.Commands
{
	public class MaxTimeCommand : DelegatingTestCommand
	{
		private int maxTime;

		public MaxTimeCommand(TestCommand innerCommand, int maxTime)
			: base(innerCommand)
		{
			this.maxTime = maxTime;
		}

		public override TestResult Execute(ITestExecutionContext context)
		{
			long timestamp = Stopwatch.GetTimestamp();
			TestResult testResult = innerCommand.Execute(context);
			long num = Stopwatch.GetTimestamp() - timestamp;
			double num3 = testResult.Duration = (double)num / (double)Stopwatch.Frequency;
			if (testResult.ResultState == ResultState.Success)
			{
				double num4 = testResult.Duration * 1000.0;
				if (num4 > (double)maxTime)
				{
					testResult.SetResult(ResultState.Failure, $"Elapsed time of {num4}ms exceeds maximum of {maxTime}ms");
				}
			}
			return testResult;
		}
	}
}
