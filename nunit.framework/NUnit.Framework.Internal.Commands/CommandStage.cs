namespace NUnit.Framework.Internal.Commands
{
	public enum CommandStage
	{
		Default,
		BelowSetUpTearDown,
		SetUpTearDown,
		AboveSetUpTearDown
	}
}
