using NUnit.Framework.Interfaces;
using System.Collections;
using System.Collections.Generic;

namespace NUnit.Framework.Internal.Builders
{
	public class ParameterDataSourceProvider : IParameterDataProvider
	{
		public bool HasDataFor(IParameterInfo parameter)
		{
			return parameter.IsDefined<IParameterDataSource>(inherit: false);
		}

		public IEnumerable GetDataFor(IParameterInfo parameter)
		{
			List<object> list = new List<object>();
			IParameterDataSource[] customAttributes = parameter.GetCustomAttributes<IParameterDataSource>(inherit: false);
			foreach (IParameterDataSource parameterDataSource in customAttributes)
			{
				foreach (object datum in parameterDataSource.GetData(parameter))
				{
					list.Add(datum);
				}
			}
			return list;
		}
	}
}
