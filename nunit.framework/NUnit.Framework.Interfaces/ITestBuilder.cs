using NUnit.Framework.Internal;
using System.Collections.Generic;

namespace NUnit.Framework.Interfaces
{
	public interface ITestBuilder
	{
		IEnumerable<TestMethod> BuildFrom(IMethodInfo method, Test suite);
	}
}
