namespace NUnit.Framework.Interfaces
{
	public enum RunState
	{
		NotRunnable,
		Runnable,
		Explicit,
		Skipped,
		Ignored
	}
}
