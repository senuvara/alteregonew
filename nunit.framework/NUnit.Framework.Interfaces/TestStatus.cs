namespace NUnit.Framework.Interfaces
{
	public enum TestStatus
	{
		Inconclusive,
		Skipped,
		Passed,
		Failed
	}
}
