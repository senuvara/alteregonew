using NUnit.Framework.Internal;
using System.Collections.Generic;

namespace NUnit.Framework.Interfaces
{
	public interface IFixtureBuilder
	{
		IEnumerable<TestSuite> BuildFrom(ITypeInfo typeInfo);
	}
}
