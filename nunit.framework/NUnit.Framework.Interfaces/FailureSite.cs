namespace NUnit.Framework.Interfaces
{
	public enum FailureSite
	{
		Test,
		SetUp,
		TearDown,
		Parent,
		Child
	}
}
