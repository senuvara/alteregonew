namespace System.Threading
{
	internal enum LazyThreadSafetyMode
	{
		None,
		PublicationOnly,
		ExecutionAndPublication
	}
}
