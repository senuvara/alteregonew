namespace NUnit.Framework.Internal
{
	public enum TestExecutionStatus
	{
		Running,
		StopRequested,
		AbortRequested
	}
}
